/**
 * Created by Lab on 22/05/2018.
 */

public class Apoio {

    private String questao;

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    private int cod;
    private String op1;
    private String op2;
    private String op3;


    public String getQuestao() {
        return questao;
    }

    public void setQuestao(String questao) {
        this.questao = questao;


    }


    public String getOp1() {
        return op1;
    }

    public void setOp1(String op1) {
        this.op1 = op1;


    }

    public String getOp2() {
        return op2;
    }

    public void setOp2(String op2) {
        this.op2 = op2;


    }

    public String getOp3() {
        return op3;
    }

    public void setOp3(String op3) {
        this.op3 = op3;


    }
}
