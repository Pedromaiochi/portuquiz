package com.example.lab.portuquiz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TelaCategoria extends AppCompatActivity implements  View.OnClickListener {

    private Button btn1, btn2, btn3,btn4, btnVoltar;
    private int acertos;
    private SharedPreferences shared;
    private TextView pontos, nivel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_categoria);

        shared = getSharedPreferences("salva", 0);

        pontos = (TextView)findViewById(R.id.textView2);
        nivel = (TextView)findViewById(R.id.textView5);

       nivel.setText( shared.getString("nivel", "1"));
       pontos.setText(shared.getString("pontos", "0"));


        btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(this);

        btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(this);

        Intent i = getIntent();
        Bundle b = i.getExtras();

        acertos = b.getInt("acertos");

        btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(this);

        btn4 = (Button) findViewById(R.id.btn4);
        btn4.setOnClickListener(this);

        btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(this);
        iniciar();
    }

    private void iniciar(){
        if(acertos<5){
            btn2.setBackgroundColor(Color.parseColor("#808080"));
            btn2.setEnabled(false);
            btn3.setBackgroundColor(Color.parseColor("#808080"));
            btn3.setEnabled(false);
            btn4.setBackgroundColor(Color.parseColor("#808080"));
            btn4.setEnabled(false);
        }else if((acertos >= 5 )&& (acertos <10))
        {
            btn1.setBackgroundColor(Color.parseColor("green"));
            btn1.setEnabled(true);
            btn2.setBackgroundColor(Color.parseColor("green"));
            btn2.setEnabled(true);
            btn3.setBackgroundColor(Color.parseColor("#808080"));
            btn3.setEnabled(false);
            btn4.setBackgroundColor(Color.parseColor("#808080"));
            btn4.setEnabled(false);
        }else if ((acertos >=10)&&(acertos <15))
        {
            btn1.setBackgroundColor(Color.parseColor("green"));
            btn1.setEnabled(true);
            btn2.setBackgroundColor(Color.parseColor("green"));
            btn2.setEnabled(true);
            btn3.setBackgroundColor(Color.parseColor("#808080"));
            btn3.setEnabled(true);
            btn4.setBackgroundColor(Color.parseColor("#808080"));
            btn4.setEnabled(false);
        }else
        {
            btn1.setBackgroundColor(Color.parseColor("green"));
            btn1.setEnabled(true);
            btn2.setBackgroundColor(Color.parseColor("green"));
            btn2.setEnabled(true);
            btn3.setBackgroundColor(Color.parseColor("#808080"));
            btn3.setEnabled(true);
            btn4.setBackgroundColor(Color.parseColor("#808080"));
            btn4.setEnabled(true);
        }
        if(acertos > 15)
        {

        }


    }
    @Override
    public void onClick(View view) {
        if (view == btnVoltar) {
            Intent i = new Intent(this, TelaInicio.class);
            this.startActivity(i);
        }

        if (view == btn1) {
            Intent i = new Intent(this, TelaJogo.class);
            Bundle bun = new Bundle();
            bun.putInt("cod", 1);
            i.putExtras(bun);
            this.startActivity(i);
        }
        if (view == btn2) {
            Intent i = new Intent(this, TelaJogo.class);
            Bundle bun = new Bundle();
            bun.putInt("cod", 2);
            i.putExtras(bun);
            this.startActivity(i);
        }

        if (view == btn3) {
            Intent i = new Intent(this, TelaJogo.class);
            Bundle bun = new Bundle();
            bun.putInt("cod", 3);
            i.putExtras(bun);
            this.startActivity(i);
        }

        if (view == btn4) {
            Intent i = new Intent(this, TelaJogo.class);
            Bundle bun = new Bundle();
            bun.putInt("cod", 4);
            i.putExtras(bun);
            this.startActivity(i);
        }
    }
}