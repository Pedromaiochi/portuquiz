package com.example.lab.portuquiz;

import java.util.ArrayList;

/**
 * Created by Lab on 14/08/2018.
 */

public class CarregarQuestoes {
    private ArrayList<Apoio> lista;

    public CarregarQuestoes(){
        lista = new ArrayList<Apoio>();

        Apoio a = new Apoio();
        a.setQuestao("Qual o plural de guarda roupa");
        a.setOp1("Guardas - Roupa");
        a.setOp2("Guardas - Roupas");
        a.setOp3("Guarda - Roupas");
        a.setCod(1);
        a.setOpcerto(3);
        lista.add(a);

        Apoio b = new Apoio();
        b.setQuestao("Qual das alternativas está incorreta");
        b.setOp1("Tico - Tecos");
        b.setOp2("Vices - Delegados");
        b.setOp3("Amores - Perfeitos");
        b.setOpcerto(2);
        b.setCod(1);
        lista.add(b);

        Apoio c = new Apoio();
        c.setQuestao("O plural dos substantivos : Couve - Flor; Pão - de - ló; Amor - Perfeito");
        c.setOp1("Couves - Flores; Pães - de - ló; Amores - Perfeitos ");
        c.setOp2("Couves - Flor; Pão - de - lós; Amores - Perfeitos ");
        c.setOp3("Couves - Flores; Pães - de - lós; Amor - Perfeitos ");
        c.setOpcerto(1);
        c.setCod(1);
        lista.add(c);

        Apoio d = new Apoio();
        d.setQuestao("Assinale a alternativa em que está correta a formação do plural");
        d.setOp1("Gavião - Gaviães ");
        d.setOp2("Atlas - Os atlas");
        d.setOp3("Fuzil - Fuziveis ");
        d.setOpcerto(2);
        d.setCod(1);
        lista.add(d);

        Apoio e = new Apoio();
        e.setQuestao("Complete a frase com o plural correspondente : Não sei quais ----- escolher");
        e.setOp1("Chapéus ");
        e.setOp2("Chapeis");
        e.setOp3("Chapéu");
        e.setOpcerto(1);
        e.setCod(1);
        lista.add(e);

        Apoio f = new Apoio();
        f.setQuestao("Assinale a alternativa em que todas as palavras estão grifadas corretamente ");
        f.setOp1("Paralisar, Pesquisar, Ironizar, Deslizar ");
        f.setOp2("Alteza, Empreza, Francesa, Miudeza");
        f.setOp3("Cuscus, Chimpanzé, Encharcar, Encher ");
        f.setOpcerto(1);
        f.setCod(1);
        lista.add(f);

        Apoio g = new Apoio();
        g.setQuestao("Assinale o item abaixo que apresenta uma palavra que está escrita de maneira errada ");
        g.setOp1("Alteza, Duqueza, Baroneza ");
        g.setOp2("Riqueza, Dureza, Fineza");
        g.setOp3("Princesa, Baixeza, Burguesa ");
        g.setOpcerto(1);
        g.setCod(2);
        lista.add(g);

        Apoio h = new Apoio();
        h.setQuestao("Assinale a opção onde o uso do O ou U está correto ");
        h.setOp1("Muela, Bulir, Taboada ");
        h.setOp2("Borbulhar, Mágoa, Regugitar");
        h.setOp3("Costume, Goela, Tabuleta ");
        h.setOpcerto(2);
        h.setCod(2);
        lista.add(h);

        Apoio i = new Apoio();
        i.setQuestao("Qual das opções abaixo está correta ");
        i.setOp1("Ascensão, Expontâneo, Privilégio ");
        i.setOp2("Encher, Enxame, Froucho, Richa");
        i.setOp3("Berinjela, Traje, Vagem, Azia");
        i.setOpcerto(3);
        i.setCod(2);
        lista.add(i);

        Apoio j = new Apoio();
        j.setQuestao("Com referência à regência do verbo assitir, todas as alternativas estão corretas, exceto : ");
        j.setOp1("Assistimos ontem a um belo filme na televisão");
        j.setOp2("Os médicos assistiram os feridos durante a guerra");
        j.setOp3("O técnico assistiu os jogadores no treino");
        j.setOpcerto(2);
        j.setCod(1);
        lista.add(j);

        Apoio k = new Apoio();
        k.setQuestao("Qual a alternativa que melhor preenche as colunas : ------ filme você assitirá à noite? ----- você aspira na vida ? ----- o candidato pretende chegar");
        k.setOp1("Que, A que, De que ");
        k.setOp2("Que, A que, A que");
        k.setOp3("Que, De que, Que");
        k.setOpcerto(2);
        k.setCod(1);
        lista.add(k);

        Apoio l = new Apoio();
        l.setQuestao("A única frase aonde a erro de concordância é :  ");
        l.setOp1("Deu seis horas no relógio da matriz ");
        l.setOp2("Devem ser duas horas e meia ");
        l.setOp3("Vai fazer cinco meses que ela se foi ");
        l.setOpcerto(3);
        l.setCod(2);
        lista.add(l);

        Apoio m = new Apoio();
        m.setQuestao("Em cada uma das séries, apenas uma palavra deve receber acento gráfico, EXCETO em :");
        m.setOp1("Bauru,Fuzil,Virus,Jovem, Automovel ");
        m.setOp2("Rubrica, Selo, Morumbi, Notavel, Modelo ");
        m.setOp3("Jovens, Itens, Polens, Hifens, Refens");
        m.setOpcerto(1);
        m.setCod(2);
        lista.add(m);

        Apoio n = new Apoio();
        n.setQuestao("Assinale a alternativa que preenche corretamente as lacunas do período : Agradeço --- Vossa Senhoria ---- oportunidade para manifestar minha opinião ---- respeito.");
        n.setOp1("A, À, A ");
        n.setOp2("À, A, À ");
        n.setOp3("A, A, A ");
        n.setOpcerto(3);
        n.setCod(3);
        lista.add(n);

        Apoio o = new Apoio();
        o.setQuestao("Qual é a alternativa incorreta quanto à concordância verbal ");
        o.setOp1("Sete quilos de carne é pouco para o churrasco");
        o.setOp2("Da minha casa até a escola é três quilômetros");
        o.setOp3("São dezenove horas");
        o.setOpcerto(2);
        o.setCod(2);
        lista.add(o);

        Apoio p = new Apoio();
        p.setQuestao("Qual é a alternativa incorreta quanto à concordância verbal ");
        p.setOp1("Sete quilos de carne é pouco para o churrasco.");
        p.setOp2("Da minha casa até a escola é três quilômetros");
        p.setOp3("São dezenove horas");
        p.setOpcerto(2);
        p.setCod(2);
        lista.add(p);

        Apoio q = new Apoio();
        q.setQuestao("Todos já descobriram a chata que ela é. A função sintática do pronome relativo na oração é :  ");
        q.setOp1("Adjunto adverbial");
        q.setOp2("Objeto direto");
        q.setOp3("Predicativo do sujeito");
        q.setOpcerto(3);
        q.setCod(1);
        lista.add(q);

        Apoio r = new Apoio();
        r.setQuestao("Assinale a alternativa que preenche as lacunas de forma correta do seguinte texto : Eles ---- cansados e ----- ao cinema.  ");
        r.setOp1("Estão, Vão");
        r.setOp2("Estam, Vam");
        r.setOp3("Estavão, Vão");
        r.setOpcerto(1);
        r.setCod(3);
        lista.add(r);

        Apoio s = new Apoio();
        s.setQuestao("Mesmo que ----- as descobertas, toda a verdade dos acontecimentos ------- . Complete as lacunas coma  alternativa correta.");
        s.setOp1("Doam, Será investigado");
        s.setOp2("Doa, Serão investigados");
        s.setOp3("Doam, Serão investigados");
        s.setOpcerto(1);
        s.setCod(1);
        lista.add(s);

        Apoio t = new Apoio();
        t.setQuestao("Na frase: “O quarto estava em perfeita ordem.”, o termo grifado se configura num(a): ");
        t.setOp1("advérbio");
        t.setOp2("pronome");
        t.setOp3("adjetivo");
        t.setOpcerto(3);
        t.setCod(1);
        lista.add(t);

        Apoio u = new Apoio();
        u.setQuestao("(Ufuscar) “O acordo não __________ as reivindicações, a não ser que __________ os nossos direitos e ____________ da luta”. A sequência correta que complementa esta oração é: ");
        u.setOp1("substitui – abdiquemos – desistamos");
        u.setOp2("substitue – abdicamos – desistimos");
        u.setOp3("substitui – abdicamos – desistimos");
        u.setOpcerto(1);
        u.setCod(3);
        lista.add(u);

        Apoio v = new Apoio();
        v.setQuestao("Complete: “As linhas ________ para um ponto e depois se _______ no infinito. ");
        v.setOp1("convergem – esvão.");
        v.setOp2("convergem – esvaem");
        v.setOp3("convirgem – esvaem");
        v.setOpcerto(2);
        v.setCod(3);
        lista.add(v);

        Apoio w = new Apoio();
        w.setQuestao(" Em qual das alternativas todos os verbos estão em tempos do pretérito? ");
        w.setOp1("Chamei-lhe a atenção porque teria observado de perto seu progresso.");
        w.setOp2(" Concordei que assim era, mas aleguei que a velhice estava agora no domínio da compensação.");
        w.setOp3(" Lembra-me de o ver erguer-se assustado e tonto.");
        w.setOpcerto(2);
        w.setCod(4);
        lista.add(w);

        Apoio x = new Apoio();
        x.setQuestao("  “Outros dois ficaram de sentinela para obstar a intervenção de algum paisano” (Carlos Drummond de Andrade).Permutando o verbo ficar por manter-se, seguindo a mesma conjugação do verbo ficar nesta frase, a resposta correta é: ");
        x.setOp1("“Outros dois mantevem-se...”");
        x.setOp2(" “Outros dois mantinham-se...”");
        x.setOp3(" “Outros dois mantiveram-se...”");
        x.setOpcerto(3);
        x.setCod(3);
        lista.add(x);

        Apoio y = new Apoio();
        y.setQuestao("   (FAAP) Assinale a resposta correspondente à alternativa que completa corretamente o espaço em branco: “Não ________. Você não acha preferível que ele se ________ sem que você o __________?” ");
        y.setOp1(" interfere – desdiz – obriga.");
        y.setOp2(" interfira – desdisser – obrigue.");
        y.setOp3(" interfira – desdiga – obrigue.");
        y.setOpcerto(3);
        y.setCod(3);
        lista.add(y);

        Apoio z = new Apoio();
        z.setQuestao("  (FUVEST-SP) Assinale a frase em que aparece o pretérito mais-que-perfeito do verbo ser: ");
        z.setOp1(" Não seria o caso de você se acusar?");
        z.setOp2(" Quando cheguei, ele já se fora, muito zangado.");
        z.setOp3(" Bem depois se soube que não fora ele o culpado.");
        z.setOpcerto(3);
        z.setCod(4);
        lista.add(z);

        Apoio a1 = new Apoio();
        a1.setQuestao("  Em todas as alternativas, a lacuna pode ser preenchida com o verbo indicado entre parênteses, no subjuntivo, exceto em: ");
        a1.setOp1(" Olhou para o cão, enquanto esperava que lhe _______ a porta (abrir).");
        a1.setOp2(" Por que foi que aquela criatura não ________ com franqueza? (proceder) ");
        a1.setOp3(" É preciso que uma pessoa se ________ para encurtar a despesa. (trancar).");
        a1.setOpcerto(2);
        a1.setCod(3);
        lista.add(a1);


        Apoio a2 = new Apoio();
        a2.setQuestao("Empregou-se o verbo no futuro do subjuntivo em: ");
        a2.setOp1("  Se algum dia a civilização ganhar essa paragem longínqua.");
        a2.setOp2(" afrontava os perigos (...) para vir vê-la à cidade. ");
        a2.setOp3(" Continuaram ainda a dialogar com certo azedume.");
        a2.setOpcerto(1);
        a2.setCod(4);
        lista.add(a2);

        Apoio a3 = new Apoio();
        a3.setQuestao("No enunciado: “Virgílio, traga-me uma coca cola bem gelada!”, registra-se uma figura de linguagem denominada: ");
        a3.setOp1(" anáfora");
        a3.setOp2(" personificação ");
        a3.setOp3(" metonímia");
        a3.setOpcerto(3);
        a3.setCod(3);
        lista.add(a3);

        Apoio a4 = new Apoio();
        a4.setQuestao(" No sintagma: “Uma palavra branca e fria”, encontramos a figura denominada: ");
        a4.setOp1("sinestesia");
        a4.setOp2("eufemismo ");
        a4.setOp3("onomatopeia");
        a4.setOpcerto(1);
        a4.setCod(4);
        lista.add(a4);

        Apoio a5 = new Apoio();
        a5.setQuestao("Considerando-se as normas de acentuação, assinalar a alternativa CORRETA: ");
        a5.setOp1("raíz");
        a5.setOp2("mêses ");
        a5.setOp3("contém");
        a5.setOpcerto(3);
        a5.setCod(1);
        lista.add(a5);

        Apoio a6 = new Apoio();
        a6.setQuestao("Assinalar a alternativa que classifica, CORRETA e respectivamente, as palavras sublinhadas em “‘Em um país racista como o nosso, a desigualdade de gênero é uma preocupação importante’...”:");
        a6.setOp1("Artigo indefinido - pronome possessivo - preposição");
        a6.setOp2("Artigo indefinido - pronome demonstrativo - preposição. ");
        a6.setOp3("Numeral - pronome possessivo - preposição");
        a6.setOpcerto(1);
        a6.setCod(4);
        lista.add(a6);

        Apoio a7 = new Apoio();
        a7.setQuestao("Assinalar a alternativa que preenche a lacuna abaixo CORRETAMENTE:\n" +
                "        Ave símbolo da Argentina, os ______________ são aqueles pássaros que conseguem manipular barro, construindo ninhos nos troncos das árvores.");
        a7.setOp1("joãos-de-barro");
        a7.setOp2("joões-de-barro");
        a7.setOp3("joões-de-barros");
        a7.setOpcerto(2);
        n.setCod(1);
        lista.add(a7);

        Apoio a8 = new Apoio();
        a8.setQuestao(" Considere o período e as afirmativas a seguir e assinale a alternativa correta.\n" +
                "                O crescimento assustador de casos de febre amarela trata-se de um sério problema e fizeram com que muitas\n" +
                "        pessoas procurassem os postos de saúde.\n" +
                "        I. O verbo “trata-se” está empregado incorretamente, pois o uso do “se” é incompatível com a presença do sujeito simples.\n" +
                "        II. Há um problema de concordância verbal: o verbo “fazer” deveria estar na terceira pessoa do singular.\n" +
                "                Estão corretas as afirmativas:");
        a8.setOp1("I e II, III");
        a8.setOp2("I, apenas.");
        a8.setOp3("I e II");
        a8.setOpcerto(3);
        a8.setCod(4);
        lista.add(a8);

        Apoio a9 = new Apoio();
        a9.setQuestao("Dê a função sintática dos termos assinalados pelas aspas: \"O lucro\", que é um dos incentivos do sistema, foi \"excelente\".");
        a9.setOp1("objeto direto - adjunto adverbial");
        a9.setOp2("sujeito - predicativo do sujeito");
        a9.setOp3("sujeito - predicativo do objeto");
        a9.setOpcerto(2);
        a9.setCod(2);
        lista.add(a9);

        Apoio a10 = new Apoio();
        a10.setQuestao("Analise a frase a seguir: Bastantes postos aumentaram o valor do combustível. Assinale a alterativa CORRETA.");
        a10.setOp1(" Na frase acima, “bastantes” assume a função de pronome indefinido, aparecendo antes de um substantivo com o qual deve concordar em número");
        a10.setOp2(" Na frase acima, “bastantes” é um advérbio de intensidade, por isso é variável.");
        a10.setOp3("Na frase acima, “bastantes” é um adjetivo, por isso deve concordar com o substantivo em número.");
        a10.setOpcerto(1);
        a10.setCod(3);
        lista.add(a10);

        Apoio a11 = new Apoio();
        a11.setQuestao("\"Pagam bem lá?\" Nesta oração o sujeito é:");
        a11.setOp1(" simples");
        a11.setOp2(" oculto.");
        a11.setOp3("indeterminado");
        a11.setOpcerto(3);
        a11.setCod(4);
        lista.add(a11);

        Apoio a12 = new Apoio();
        a12.setQuestao("\"Afinal, lá se está sempre contente.\" Nesta oração o tipo de sujeito é:");
        a12.setOp1("indeterminado");
        a12.setOp2(" composto");
        a12.setOp3(" oculto");
        a12.setOpcerto(1);
        a12.setCod(1);
        lista.add(a12);


        Apoio a13 = new Apoio();
        a13.setQuestao("\"Precisa-se de operários para a obra.\" Nesta oração o tipo de sujeito é:");
        a13.setOp1(" composto");
        a13.setOp2(" indeterminado");
        a13.setOp3(" simples");
        a13.setOpcerto(2);
        a13.setCod(2);
        lista.add(a13);


        Apoio a14 = new Apoio();
        a14.setQuestao("\"Os livros escolares devem ser tratados com carinho.\" Nesta oração o tipo de sujeito é:");
        a14.setOp1(" simples ");
        a14.setOp2(" composto");
        a14.setOp3(" indeterminado");
        a14.setOpcerto(1);
        a14.setCod(4);
        lista.add(a14);


        Apoio a15 = new Apoio();
        a15.setQuestao("Quanto à espécie, o sujeito de uma oração pode ser:");
        a15.setOp1(" Simples, composto ou implícito  ");
        a15.setOp2(" Determinado ou indeterminado");
        a15.setOp3(" As duas alternativas anteriores estão corretas");
        a15.setOpcerto(3);
        a15.setCod(1);
        lista.add(a15);

        Apoio a16 = new Apoio();
        a16.setQuestao("Nem sempre, porém, menos gelo significa más notícias.\n" +
                "        A conjunção grifada acima pode ser substituída, sem alteração do sentido original, por:\n" +
                "        enquanto.por isso.ainda que.senão.no entanto.");
        a16.setOp1(" ainda que ");
        a16.setOp2(" no entanto. ");
        a16.setOp3("  enquanto");
        a16.setOpcerto(2);
        a16.setCod(4);
        lista.add(a16);


        Apoio a17 = new Apoio();
        a17.setQuestao("O sujeito de uma oração é determinado quando:");
        a17.setOp1(" O seu núcleo é sempre um substantivo ");
        a17.setOp2(" O seu núcleo é um substantivo, palavra substantivada, pronome ou oração substantiva");
        a17.setOp3(" O seu núcleo é sempre uma oração substantiva ou um substantivo");
        a17.setOpcerto(2);
        a17.setCod(3);
        lista.add(a17);


        Apoio a18 = new Apoio();
        a18.setQuestao("Em relação às vogais e semivogais, marcar C para as afirmativas Certas, E para as Erradas e, após, assinalar a alternativa que apresenta a sequência CORRETA:\n" +
                "        () Nas palavras “quatro” e “igualdade”, o /u/ é uma semivogal.\n" +
                "        () Nas palavras “vereadores” e “atuação”, o /a/ é uma semivogal.\n" +
                "        () Na palavra “socióloga”, o /i/ é uma vogal.");
        a18.setOp1("C - E - C");
        a18.setOp2(" E - C - C");
        a18.setOp3(" C - E - E");
        a18.setOpcerto(1);
        a18.setCod(4);
        lista.add(a18);


        Apoio a19 = new Apoio();
        a19.setQuestao(" A frase em que a palavra destacada está flexionada de acordo com a norma-padrão da língua portuguesa é:");
        a19.setOp1("Se os moradores obterem lâmpadas modernas para iluminar suas casas, farão economia de eletricidade.");
        a19.setOp2(" Se você ver águas paradas, tome uma providência para evitar a proliferação do mosquito.");
        a19.setOp3(" Para comunicar a seus acionistas o resultado financeiro semestral, o relatório abrangeu os aspectos principais relacionados à produção da empresa.");
        a19.setOpcerto(3);
        a19.setCod(2);
        lista.add(a19);


        Apoio a20 = new Apoio();
        a20.setQuestao(" A frase em que a palavra destacada está flexionada de acordo com a norma-padrão da língua portuguesa é:");
        a20.setOp1("Se os moradores obterem lâmpadas modernas para iluminar suas casas, farão economia de eletricidade.");
        a20.setOp2(" Se você ver águas paradas, tome uma providência para evitar a proliferação do mosquito.");
        a20.setOp3(" Para comunicar a seus acionistas o resultado financeiro semestral, o relatório abrangeu os aspectos principais relacionados à produção da empresa.");
        a20.setOpcerto(3);
        a20.setCod(2);
        lista.add(a20);

        Apoio a21 = new Apoio();
        a21.setQuestao(" Em “no escritório transformo o mundo com telefonemas, projetos e papéis.”, empregando-se o verbo em destaque na voz passiva pronominal, tem-se: “no escritório transforma-se o mundo com telefonemas, projetos e papéis.”\n" +
                "        Essa transformação confere ao trecho um caráter");
        a21.setOp1("ambíguo.");
        a21.setOp2(" impessoal");
        a21.setOp3(" coloquial");
        a21.setOpcerto(2);
        a21.setCod(3);
        lista.add(a21);

        Apoio a22 = new Apoio();
        a22.setQuestao("Empregou-se corretamente a regência verbal em:");
        a22.setOp1("desgastada - caras");
        a22.setOp2("desgastado - caro");
        a22.setOp3("desgastados - cara");
        a22.setOpcerto(3);
        a22.setCod(1);
        lista.add(a22);

        Apoio a23 = new Apoio();
        a23.setQuestao(" Assinale a opção cujo complemento verbal tem a função de objeto indireto.");
        a23.setOp1("Preveniram antecipadamente os jovens do perigo");
        a23.setOp2("As crianças menores acreditam em contos de fada");
        a23.setOp3("Maria comprou um carro novo.");
        a23.setOpcerto(2);
        a22.setCod(2);
        lista.add(a23);

        Apoio a24 = new Apoio();
        a24.setQuestao(" “Fé” é uma palavra:");
        a24.setOp1(" Monossílaba");
        a24.setOp2("Dissílaba");
        a24.setOp3("Trissílaba");
        a24.setOpcerto(1);
        a24.setCod(1);
        lista.add(a24);

        Apoio a25 = new Apoio();
        a25.setQuestao(" “Fé” é uma palavra:");
        a25.setOp1(" Monossílaba");
        a25.setOp2("Dissílaba");
        a25.setOp3("Trissílaba");
        a25.setOpcerto(1);
        a25.setCod(1);
        lista.add(a25);

        Apoio a26 = new Apoio();
        a26.setQuestao(" Assinale a opção que completa corretamente as lacunas do período abaixo." +
                "Agora que há uma câmera de _______, isto provavelmente não _______ acontecerá, mas _______ vezes em que, no meio de uma noite _______, o poeta levantava de seu banco [...]");
        a26.setOp1(" investigação mais houve chuvosa");
        a26.setOp2("investigassão mais houve chuvoza");
        a26.setOp3(" investigassão mas ouve chuvosa");
        a26.setOpcerto(1);
        a26.setCod(4);
        lista.add(a26);


        Apoio a27 = new Apoio();
        a27.setQuestao("Entre os substantivos abaixo relacionados, indique o masculino:");
        a27.setOp1(" Cal");
        a27.setOp2(" Omelete");
        a27.setOp3(" Guaraná");
        a27.setOpcerto(3);
        a27.setCod(3);
       lista.add(a27);

        Apoio a28 = new Apoio();
        a28.setQuestao("O plural da palavra “ilusão” é:");
        a28.setOp1(" ilusãos");
        a28.setOp2(" ilusões");
        a28.setOp3(" ilusães");
        a28.setOpcerto(2);
        a28.setCod(1);
        lista.add(a28);

        Apoio a29 = new Apoio();
        a29.setQuestao("Em qual das alternativas abaixo as sílabas da palavra “ameixa” foram\n" +
                "        divididas corretamente:");
        a29.setOp1("  a – mei – xa");
        a29.setOp2("  ame – ixa");
        a29.setOp3(" ame – i – xa");
        a29.setOpcerto(1);
        a29.setCod(2);
        lista.add(a29);

        Apoio a30 = new Apoio();
        a30.setQuestao("Indique a alternativa em que o termo sublinhado não é um diminutivo sintético:");
        a30.setOp1(" No meio da estrada havia quatro burricos empacados");
        a30.setOp2(" Tomaremos banho naquele riacho");
        a30.setOp3(" Nessa festa junina foi feito um fogaréu");
        a30.setOpcerto(3);
        a30.setCod(2);
        lista.add(a30);



    }

    public ArrayList<Apoio> getLista(){
        return lista;
    }

    public void setLista(ArrayList<Apoio> lista){
        this.lista = lista;
    }
}
