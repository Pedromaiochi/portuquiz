package com.example.lab.portuquiz;

/**
 * Created by Lab on 15/05/2018.
 */

public class Apoio {

    private String questao, op1, op2, op3;
    private int cod;
    private int opcerto;

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getQuestao() {
        return questao;
    }

    public void setQuestao(String questao) {
        this.questao = questao;
    }


    public String getOp1() {
        return op1;
    }

    public void setOp1(String op1) {
        this.op1 = op1;


    }

    public String getOp2() {
        return op2;
    }

    public void setOp2(String op2) {
        this.op2 = op2;


    }

    public String getOp3() {
        return op3;
    }

    public void setOp3(String op3) {
        this.op3 = op3;


    }

    public int getOpcerto() {
        return opcerto;
    }

    public void setOpcerto(int opcerto) {
        this.opcerto = opcerto;
    }








}

