package com.example.lab.portuquiz;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class TelaJogo extends Activity implements View.OnClickListener {

    private Button btnProximo, btn1, btn2, btn3, btnVoltar;
    private  int quemEstaComOCerto;
    private ArrayList<Apoio>lista;
    private TextView texto, pontuacao, nivel;
    private int pontos, cont, cod;
    private SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_jogo);

        shared = getSharedPreferences("salva", 0);

        btnProximo = (Button) findViewById(R.id.btnProximo);
        btnProximo.setOnClickListener(this);

        btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(this);

        btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(this);

        btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(this);

        btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(this);

        texto= (TextView) findViewById(R.id.textView);
        pontuacao= (TextView) findViewById(R.id.txtPontuacao);
        nivel = (TextView) findViewById(R.id.txtNivel);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        cod = b.getInt("cod");

        nivel.setText(Integer.toString(cod));

        lista = new ArrayList<Apoio>();

        Log.d("cod", ""+cod);

        pontos = 0;
        cont = 0;



        lista = new ArrayList<Apoio>();
        CarregarQuestoes c = new CarregarQuestoes();

        lista = c.getLista();

        iniciarJogo();
    }

    private void acertou(){
        pontos++;

        pontuacao.setText(Integer.toString(pontos));
        if(pontos % 5 == 0){
            if(pontos == 20 ){
                cod++;
                texto.setText("Parabéns, você ganhou o jogo");
                cont = 0;
                nivel.setVisibility(View.INVISIBLE);
            }
            else {
                cod++;
                texto.setText("Parabéns, você passou para a fase"+ cod);
                cont = 0;
                nivel.setText(Integer.toString(cod));
            }
            btn1.setVisibility(View.INVISIBLE);
            btn2.setVisibility(View.INVISIBLE);
            btn3.setVisibility(View.INVISIBLE);
        }

    }

    private void proximaQuestao(){
        btn1.setBackgroundColor(Color.WHITE);
        btn2.setBackgroundColor(Color.WHITE);
        btn3.setBackgroundColor(Color.WHITE);

        btn1.setVisibility(View.VISIBLE);
        btn2.setVisibility(View.VISIBLE);
        btn3.setVisibility(View.VISIBLE);

        btn2.setEnabled(true);
        btn1.setEnabled(true);
        btn3.setEnabled(true);

        boolean prosseguir = false;

        do{
            if(cod == lista.get(cont).getCod()){
                texto.setText(lista.get(cont).getQuestao());

                btn1.setText(lista.get(cont).getOp1());
                btn2.setText(lista.get(cont).getOp2());
                btn3.setText(lista.get(cont).getOp3());
                quemEstaComOCerto = lista.get(cont).getOpcerto();
                prosseguir = true;
            }else{
                cont++;
            }
        }while(!prosseguir);
        cont++;
    }

    private void iniciarJogo() {
        Collections.shuffle(lista);
        proximaQuestao();
    }



    @Override
    public void onClick(View view) {
        if (view == btnProximo){
            proximaQuestao();
        }

        if (view == btnVoltar){
           Intent i = new Intent(this, TelaCategoria.class);
                Bundle b = new Bundle();
                b.putInt("acertos", pontos);
                i.putExtras(b);

                SharedPreferences.Editor gravador = shared.edit();
                gravador.putString("nivel", nivel.getText().toString() );
                gravador.putString("pontos", pontuacao.getText().toString());
                gravador.commit();

            startActivity(i);
        }

        if (view == btn1){
            if (quemEstaComOCerto == 1){
                btn1.setBackgroundColor(Color.GREEN);
                btn2.setEnabled(false);
                btn3.setEnabled(false);
                acertou();
            }else{
                btn1.setVisibility(View.INVISIBLE);
            }
        }

        if (view == btn2){
            if (quemEstaComOCerto == 2){
                btn2.setBackgroundColor(Color.GREEN);
                btn1.setEnabled(false);
                btn3.setEnabled(false);
                acertou();
            }else{
                btn2.setVisibility(View.INVISIBLE);
            }
        }

        if (view == btn3){
            if (quemEstaComOCerto == 3){
                btn3.setBackgroundColor(Color.GREEN);
                btn2.setEnabled(false);
                btn1.setEnabled(false);
                acertou();
            }else{
                btn3.setVisibility(View.INVISIBLE);
            }
        }
}
}