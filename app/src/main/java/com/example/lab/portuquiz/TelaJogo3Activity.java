package com.example.lab.portuquiz;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TelaJogo3Activity extends Activity implements View.OnClickListener{



    private Button btnProximo, btnMenu;
    private TextView texto,pontuacao, nivel;
    private EditText resposta;
    private ArrayList<Apoio> lista;
    private int cont, cod, pontos;
    private SharedPreferences shared;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_jogo_3);

        btnProximo = (Button)findViewById(R.id.button3);
        btnProximo.setOnClickListener(this);

        btnMenu = (Button)findViewById(R.id.button2);
        btnMenu.setOnClickListener(this);

        shared = getSharedPreferences("salva", 0);

        texto = (TextView) findViewById(R.id.textView14);

        pontuacao = (TextView) findViewById(R.id.textView13);

        nivel = (TextView) findViewById(R.id.textView11);


        Intent i = getIntent();
        Bundle b= i.getExtras();



        cod = b.getInt("cod");

        nivel.setText(Integer.toString(cod));

        lista = new ArrayList<Apoio>();
        CarregarQuestoes c= new CarregarQuestoes();

        Log.d("cod", ""+cod);

        pontos = 0;
        cod = 0;

        lista = c.getLista();

        iniciarJogo();


    }

    private void iniciarJogo() {



    }

    private void proximaQuestao() {

    }


    @Override
    public void onClick(View v) {
        if (v == btnProximo){
            proximaQuestao();
        }

        if (v == btnMenu){
            Intent i = new Intent (this, TelaCategoria.class);
            Bundle b = new Bundle();
            b.putInt("acerto", pontos);
            i.putExtras(b);

            SharedPreferences.Editor gravador = shared.edit();
            gravador.putString("nivel", nivel.getText().toString());
            gravador.putString("pontos", pontuacao.getText().toString());
            gravador.commit();

            startActivity(i);

        }


    }
}

