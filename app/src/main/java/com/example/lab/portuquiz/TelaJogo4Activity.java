package com.example.lab.portuquiz;

import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class TelaJogo4Activity extends AppCompatActivity implements View.OnClickListener {

    private TextView txt1, txt2, txt3, txt4, texto, pontuacao, nivel;
    private ArrayList<Apoio> lista;
    private int cont, cod, pontos;
    private SharedPreferences shared;
    private Button btnProximo, btnMenu;



    @Override


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_jogo_4);

        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        txt3 = (TextView) findViewById(R.id.txt3);
        txt4 = (TextView) findViewById(R.id.textView18);

        txt1.setOnLongClickListener(longClickListener);
        txt2.setOnLongClickListener(longClickListener);
        txt3.setOnLongClickListener(longClickListener);

        txt4.setOnDragListener(dragListener);

        btnProximo = (Button) findViewById(R.id.button3);
        btnProximo.setOnClickListener(this);

        btnMenu = (Button) findViewById(R.id.button2);
        btnMenu.setOnClickListener(this);

        shared = getSharedPreferences("salva", 0);

        texto = (TextView) findViewById(R.id.textView14);

        pontuacao = (TextView) findViewById(R.id.textView13);

        nivel = (TextView) findViewById(R.id.textView11);


        Intent i = getIntent();
        Bundle b = i.getExtras();


        cod = b.getInt("cod");

        nivel.setText(Integer.toString(cod));

        lista = new ArrayList<Apoio>();
        CarregarQuestoes c = new CarregarQuestoes();

        Log.d("cod", "" + cod);

        pontos = 0;
        cod = 0;

        lista = c.getLista();


    private void iniciarJogo() {
    }



    private void proximaQuestao() {
    }




    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder myShadowBuilder = new View.DragShadowBuilder(v);
            v.startDrag(data, myShadowBuilder, v, 0);

            return true;
        }
    };

    View.OnDragListener dragListener = new View.OnDragListener() {




    public boolean onDrag(View v, DragEvent event) {
        int dragEvent = event.getAction();
        final View view = (View) event.getLocalState();
        switch (dragEvent) {
            case DragEvent.ACTION_DRAG_ENTERED:

                if (view.getId() == R.id.txt1) {
                    txt4.setText("O texto 1 esta arrastado");
                } else if (view.getId() == R.id.txt2) {
                    txt4.setText("O texto 2 esta arrastado");

                } else if (view.getId() == R.id.txt3) {
                    txt4.setText("O texto 3 esta arrastado");
                }
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                if (view.getId() == R.id.txt1) {
                    txt4.setText("O texto 1 esta saindo");
                } else if (view.getId() == R.id.txt2) {
                    txt4.setText("O texto 2 esta saindo");

                } else if (view.getId() == R.id.txt3) {
                    txt4.setText("O texto 3 esta saindo");
                }

                break;
            case DragEvent.ACTION_DROP:

                if (view.getId() == R.id.txt1) {
                    txt4.setText("O texto 1 esta saindo");


                    view.animate()
                            .x(txt4.getX())
                            .y(txt4.getY())
                            .setDuration(700)
                            .start();

                    break;


                }
                return true;

        }
        return false;
    }




    @Override
    public void onClick(View v) {
        if (v == btnProximo) {
            proximaQuestao();
        }

        if (v == btnMenu) {
            Intent i = new Intent( this, TelaCategoria.class);
            Bundle b = new Bundle();
            b.putInt("acerto", pontos);
            i.putExtras(b);

            SharedPreferences.Editor gravador = shared.edit();
            gravador.putString("nivel", nivel.getText().toString());
            gravador.putString("pontos", pontuacao.getText().toString());
            gravador.commit();

            startActivity(i);

        }


    }



}

