package com.example.lab.portuquiz;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaInicio extends Activity implements View.OnClickListener {

    private Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicio);

        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == btnEntrar){
            Intent i = new Intent(this, TelaCategoria.class);
            Bundle b = new Bundle();
            b.putInt("acertos", 1);
            i.putExtras(b);
            startActivity(i);

        }
    }
}
